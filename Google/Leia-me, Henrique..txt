Resumo:

Com Dono da Bola voc� organiza sua partida e seu time amador de maneira simples.

Descri��o:

O Dono da Bola � um gerenciador de esportes coletivos para o organizador do esporte em quest�o. Dentre as suas fun��es ele deve conter a lista de todos os participantes, fazer o controle de presen�a dos participantes nos eventos, gerenciar o pagamento dos participantes, marcar o tempo do jogo, marcar os pontos ou gols e montar os times de acordo com o n�vel dos jogadores.

Politica de uso:
https://drive.google.com/file/d/10jPeZZiEkBCDbyykp9_8AP6qWwIIh56-/view

Link do Bitbucket:
https://bitbucket.org/joaosilveirab/donodabola
