var app = angular.module('donodabola', ['ionic', 'firebase','ngCordova']);

app.run(function ($ionicPlatform) {
  $ionicPlatform.ready(function () {
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});

app.config(['$ionicConfigProvider', function($ionicConfigProvider) {
  
      $ionicConfigProvider.tabs.position('bottom'); // other values: top
      $ionicConfigProvider.navBar.alignTitle('center');
}]);

app.config(function ($stateProvider, $urlRouterProvider) {

  $stateProvider.state('cadastro',
    {
      url: '/cadastro',
      templateUrl: 'templates/cadastro.html',
      controller: 'CadastroCtrl'
    });

  $stateProvider.state('login',
    {
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: 'LoginCtrl'
    });

  $stateProvider.state('tabs',
    {
      url: '/tabs',
      abstract: true,
      templateUrl: 'templates/tabs.html',
      // controller: 'LoginCtrl'
    });
    
    $stateProvider.state('tabs.events',
    {
      url: '/events',
      views: {
        "tab-events": {
          templateUrl: 'templates/events.html',
          controller: 'EventCtrl'
        }
      }
    });

    $stateProvider.state('tabs.alerts',
    {
      url: '/alerts',
      views: {
        "tab-alerts": {
          templateUrl: 'templates/alerts.html',
          controller: 'AlertsCtrl'
        }
      }
    });

    $stateProvider.state('tabs.settings',
    {
      url: '/settings',
      views: {
        "tab-settings": {
          templateUrl: 'templates/settings.html',
          controller: 'SettingCtrl'
        }
      }
    });

   $stateProvider.state('cadastroGrupo',
    {
      url: '/cadastroGrupo',
      templateUrl: 'templates/cadastroGrupo.html',
      controller: 'CadastroGrupoCtrl'
    });

    $stateProvider.state('editar', {
      url: '/edita/:id',
      templateUrl: 'templates/cadastroGrupo.html',
      controller: 'EditaCtrl'
    });

    $stateProvider.state('eventHome', {
      url: '/eventHome/:id',
      templateUrl: 'templates/eventHome.html',
      controller: 'eventHomeCtrl'
    });

    $stateProvider.state('timesPartida', {
      url: '/timesPartida/:id',
      templateUrl: 'templates/timesPartida.html',
      controller: 'timesPartidaCtrl'
    });

    $stateProvider.state('criaTimesPartida', {
      url: '/criaTimesPartida/:id',
      templateUrl: 'templates/criaTimesPartida.html',
      controller: 'criaTimesPartidaCtrl'
    });

    $stateProvider.state('confrontosPartida', {
      url: '/confrontosPartida/:id',
      templateUrl: 'templates/confrontosPartida.html',
      controller: 'confrontosPartidaCtrl'
    });

    $stateProvider.state('eventPlayers', {
      url: '/eventPlayers/:id',
      templateUrl: 'templates/eventPlayers.html',
      controller: 'eventPlayersCtrl'
    });

    $stateProvider.state('editPlayers', {
      url: '/editPlayers/:id',
      templateUrl: 'templates/cadastroJogador.html',
      controller: 'editPlayersCtrl'
    });

    $stateProvider.state('pagamentosPlayer', {
      url: '/pagamentosPlayer/:id',
      templateUrl: 'templates/pagamentosPlayer.html',
      controller: 'pagamentosPlayerCtrl'
    });

    $stateProvider.state('eventPartidas', {
      url: '/eventPartidas/:id',
      templateUrl: 'templates/eventPartidas.html',
      controller: 'eventPartidasCtrl'
    });

    $stateProvider.state('criarConfronto', {
      url: '/criarConfronto/:id',
      templateUrl: 'templates/criarConfronto.html',
      controller: 'criarConfrontoCtrl'
    });

    $stateProvider.state('presencaPartida', {
      url: '/presencaPartida/:id',
      templateUrl: 'templates/presencaPartida.html',
      controller: 'presencaPartidaCtrl'
    });

    $stateProvider.state('addPresencaPartida', {
      url: '/addPresencaPartida/:id',
      templateUrl: 'templates/addPresencaPartida.html',
      controller: 'addPresencaPartidaCtrl'
    });

    $stateProvider.state('partidaHome', {
      url: '/partidaHome/:id',
      templateUrl: 'templates/partidaHome.html',
      controller: 'partidaHomeCtrl'
    });

    $stateProvider.state('cadastroJogador', {
      url: '/cadastroJogador/:id',
      templateUrl: 'templates/cadastroJogador.html',
      controller: 'cadastroJogadorCtrl'
    });

    $stateProvider.state('inicio',
    {
      url: '/inicio',
      templateUrl: 'templates/inicio.html',
      // controller: 'InicioCrtl'
    });

  $urlRouterProvider.otherwise('inicio');

})

app.controller('cadastroJogadorCtrl', function($scope,$state,$firebaseObject, $firebaseArray,$stateParams,$cordovaCamera){
  var id = $stateParams.id;

  // $scope.pictureUrl = 'http://placehold.it/300x300';
  
  //     $scope.tirarFoto = function(){
  //         var options = {
  //           destinationType: Camera.DestinationType.DATA_URL,
  //           encodingType: Camera.EncodingType.JPEG
  //         }
  
  //       $cordovaCamera.getPicture(options)
  //         .then(function (data) {
  //           console.log('camera Data: ' * angular.toJson(data));
  //           $scope.pictureUrl = 'data:image/jpeg;base64,' + data;
  //         }, function (error) {
  //           console.log('camera Error: ' * angular.toJson(data));
  //         });
  //     };
  document.addEventListener("deviceready", function () {
    
    $scope.takePicture = function() {
      var options = {
          quality : 75,
          destinationType : Camera.DestinationType.DATA_URL,
          sourceType : Camera.PictureSourceType.CAMERA,
          allowEdit : true,
          encodingType: Camera.EncodingType.JPEG,
          targetWidth: 300,
          targetHeight: 300,
          popoverOptions: CameraPopoverOptions,
          saveToPhotoAlbum: false
      };

      $cordovaCamera.getPicture(options).then(function(imageData) {
          $scope.imgURI = "data:image/jpeg;base64," + imageData;
      }, function(err) {
          // An error occured. Show a message to the user
      });
    }
    
  }, false);;
   
  $scope.salvarJogador = function(jogador){
      var obj = jogador;
      obj.group_uid = id; 
      obj.data = obj.data.getTime();
      var ref = firebase.database().ref().child('Jogadores');
      $firebaseArray(ref).$add(obj);
    
      $state.go('eventPlayers', {id: id});
  }

})



app.controller('editPlayersCtrl', function($scope,$state,$firebaseObject, $firebaseArray,$stateParams,$cordovaCamera){
  var id = $stateParams.id;
  var ref = firebase.database().ref().child('Jogadores').child(id);
  var obj = $firebaseObject(ref);
   obj.$loaded().then(function(){
     obj.data = new Date(obj.data);
     $scope.jogador = obj;
  }) 

  //$scope.jogador = $firebaseObject(ref);

  document.addEventListener("deviceready", function () {
    
    $scope.takePicture = function() {
      var options = {
          quality : 75,
          destinationType : Camera.DestinationType.DATA_URL,
          sourceType : Camera.PictureSourceType.CAMERA,
          allowEdit : true,
          encodingType: Camera.EncodingType.JPEG,
          targetWidth: 300,
          targetHeight: 300,
          popoverOptions: CameraPopoverOptions,
          saveToPhotoAlbum: false
      };

      $cordovaCamera.getPicture(options).then(function(imageData) {
          $scope.imgURI = "data:image/jpeg;base64," + imageData;
      }, function(err) {
          // An error occured. Show a message to the user
      });
    }
    
  }, false);;

  

  $scope.salvarJogador = function(jogador){
    
    jogador.data = jogador.data.getTime();
    $scope.jogador = jogador;
    $scope.jogador.$save();
    $state.go('eventPlayers', {id: jogador.group_uid});
  }

})

app.controller('eventHomeCtrl', function($scope,$state,$firebaseObject,$firebaseArray,$stateParams,$firebaseAuth){
  var id = $stateParams.id;
  var ref = firebase.database().ref().child('Groups').child(id);

  $scope.grupo = $firebaseObject(ref);


  $scope.editar = function(id){
    $state.go('editar', {id: id});
  }

  $scope.acessarJogadores = function(id){
    $state.go('eventPlayers', {id: id});
  }

  $scope.acessarPartidas = function(id){
    $state.go('eventPartidas', {id: id});
  }

  $scope.meusEventos = function(){
    $state.go('tabs.events');
  }
})

app.controller('partidaHomeCtrl', function($scope,$state,$firebaseObject,$stateParams){
  var id = $stateParams.id;
  var ref = firebase.database().ref().child('Partidas').child(id);
  
  $scope.partida = $firebaseObject(ref);

  $scope.acessarPresenca =  function(id_partida){
    $state.go('presencaPartida', {id: id_partida});
  }

  $scope.acessarTimes =  function(id_partida){
    $state.go('timesPartida', {id: id_partida});
  }

  $scope.acessarConfrontos =  function(id_partida){
    $state.go('confrontosPartida', {id: id_partida});
  }
})

app.controller('presencaPartidaCtrl', function($scope,$state,$firebaseObject,$firebaseArray,$stateParams){
  var id = $stateParams.id;
  var ref = firebase.database().ref().child('Presencas');
  var query = ref.orderByChild('partida_uid').equalTo(id);

  var refPart = firebase.database().ref().child('Partidas').child(id);
  $scope.partida = $firebaseObject(refPart);
  
  $scope.Jogadores = $firebaseArray(query);

  $scope.addPresenca =  function(id_partida){
    $state.go('addPresencaPartida', {id: id_partida});
  }

  $scope.apagar = function(id){
    //1.Recuperar o objeto pelo id
    var obj = $scope.Jogadores.$getRecord(id);

    //2.Exclusão pelo objeto
    $scope.Jogadores.$remove(obj);
  }

})

app.controller('addPresencaPartidaCtrl', function($scope,$state,$firebaseObject,$firebaseArray,$stateParams,$ionicPopup, $timeout){
  var id = $stateParams.id;
  var ref = firebase.database().ref().child('Partidas').child(id);
  
  var partida = $firebaseObject(ref);
  partida.$loaded().then(function(){
    var refTeam = firebase.database().ref().child('Jogadores')
    var query = refTeam.orderByChild('group_uid').equalTo(partida.evento_uid);
  
    $scope.Jogadores = $firebaseArray(query);
  })

  $scope.addPresenca = function(Jogador){
    
    Jogador.partida_uid = id;
    Jogador.id_jogador = Jogador.$id;

    var verJog = firebase.database().ref().child('Presencas');
    var queryJog = verJog.orderByChild('id_jogador').equalTo(Jogador.$id);
    var check =  $firebaseArray(queryJog);
    check.$loaded().then(function(){ 
      var count = 0;
      angular.forEach(check, function(value1, key1) {
        if(value1.partida_uid === id){
          count = count + 1;
        }
      })
      if(count <= 0){
        var ref = firebase.database().ref().child('Presencas');
        $firebaseArray(ref).$add(Jogador);
      
        $state.go('presencaPartida', {id: id});
      }else{
        $ionicPopup.alert({
          title: 'OPS!',
          template: 'Jogador ja possui presenca!'
        });
     
        $state.go('presencaPartida', {id: id});
      }
    })
  }

}) 




app.controller('timesPartidaCtrl', function($scope,$state,$firebaseObject,$firebaseArray,$stateParams,$ionicPopup, $timeout){
  var id = $stateParams.id;
  var ref = firebase.database().ref().child('Partidas').child(id);
  
  $scope.partida = $firebaseObject(ref);

  var refTeam = firebase.database().ref().child('Times')
  var query = refTeam.orderByChild('partida_uid').equalTo(id);

  $scope.Times = $firebaseArray(query);

  $scope.criarTimes =  function(id_partida){
    $state.go('criaTimesPartida', {id: id_partida});
  }


 $scope.showConfirm = function(jog,id_time) {
  var confirmPopup = $ionicPopup.confirm({
    title: 'Atenção!',
    template: 'Você irá remover esse jogador do time!',
    buttons: [
      { text: '<b>Cancelar</b>',
        type: 'button-assertive'},
        {
          text: '<b>OK</b>',
          type: 'button-positive',
          onTap: function(e) {
            var players = firebase.database().ref().child('Times').child(id_time).child('Jogadores');
            var query =  players.orderByChild('nome').equalTo(jog.nome);
          
            var Jogadores = $firebaseArray(query);
      
            Jogadores.$loaded().then(function(){
             var obj2 = Jogadores.$getRecord(Jogadores[0].$id);
             Jogadores.$remove(obj2);
            })
          }
        }
      ]
  });
};

  $scope.apagar = function(id){
    //1.Recuperar o objeto pelo id
    var obj = $scope.Times.$getRecord(id);

    //2.Exclusão pelo objeto
    $scope.Times.$remove(obj);
  }


  $scope.showPopup = function(id_time) {
    $scope.data = {};

    var refTeam = firebase.database().ref().child('Presencas')
    var query = refTeam.orderByChild('partida_uid').equalTo(id);
    $scope.Jogadores = $firebaseArray(query);
  
    // An elaborate, custom popup
    var myPopup = $ionicPopup.show({
      template: '<center><select ng-model="data.jogador" required> <option ng-repeat="jogador in Jogadores">{{jogador.nome}} {{jogador.sobrenome}} ({{jogador.nick}})</option></select></center>',
      title: 'Jogadores',
      subTitle: 'Selecione um Jogador',
      scope: $scope,
      buttons: [
        { text: '<b>Cancelar</b>',
          type: 'button-assertive'},
        {
          text: '<b>Adicionar</b>',
          type: 'button-positive',
          onTap: function(e) {
            var ref = firebase.database().ref().child('Times').child(id_time).child('Jogadores');

            var jogs = $firebaseArray(ref);

            jogs.$loaded().then(function(){
              var i = 0;
              angular.forEach(jogs, function(value1, key1) {
                if(value1.nome === $scope.data.jogador){
                  i = i +1;
                } 
              })
              if(i > 0){
                $ionicPopup.alert({
                  title: 'OPS!',
                  template: 'O jogador selecionado ja esta no time!'
                });
              }else{
                var obj = {
                  nome: $scope.data.jogador
                }
                jogs.$add(obj);
              }
            }) 
          }
        }
      ]
    });
  
    // myPopup.then(function(res) {
    //   console.log('Tapped!', res);
    // });
  
    $timeout(function() {
       myPopup.close(); //close the popup after 3 seconds for some reason
    }, 30000);
   };
})

app.controller('criaTimesPartidaCtrl', function($scope,$state,$firebaseObject,$firebaseArray,$stateParams){

  var id = $stateParams.id;
  var refTeam = firebase.database().ref().child('Presencas')
  var query = refTeam.orderByChild('partida_uid').equalTo(id);
  $scope.Jogadores = $firebaseArray(query);


  $scope.salvarTime = function(Time){
    
    Time.partida_uid = id;

    var ref = firebase.database().ref().child('Times');
    $firebaseArray(ref).$add(Time);
  
    $state.go('timesPartida', {id: id});
}

})
app.controller('confrontosPartidaCtrl', function($scope,$state,$firebaseObject,$firebaseArray,$stateParams){
  var id = $stateParams.id;
  var ref = firebase.database().ref().child('Confrontos');
  var query = ref.orderByChild('partida_uid').equalTo(id);

  $scope.partida = {
    id:id
  }

  $scope.Confrontos = $firebaseArray(query);

  $scope.criarConfronto =  function(id_partida){
    $state.go('criarConfronto', {id: id_partida});
  }

  $scope.apagar = function(id){
    //1.Recuperar o objeto pelo id
    var obj = $scope.Confrontos.$getRecord(id);

    //2.Exclusão pelo objeto
    $scope.Confrontos.$remove(obj);
  }

})

app.controller('criarConfrontoCtrl', function($scope,$state,$firebaseObject,$firebaseArray,$interval,$stateParams,$ionicPopup, $timeout){
  var id = $stateParams.id;
  var ref = firebase.database().ref().child('Times');
  var query = ref.orderByChild('partida_uid').equalTo(id);
  $scope.Times = $firebaseArray(query);

  $ionicPopup.alert({
    title: 'Atenção!',
    template: 'Não selecione times que possuam jogadores iguais pois não será possivel salvar o confronto!'
  });

  ////

  // Estado do cronômetro
  var running = false;
  
  // Controle de incremento
  var lastTimestamp;

  // Handler do $interval
  var intervalHandler;

  // Contagem total de milisegundos do cronômetro
  var milliseconds = 0;

  // Bind dos valores do cronômetro
  $scope.minutos = '00';
  $scope.segundos = '00';
  $scope.startStopBtnText = 'INICIAR';

  // Função chamada ao clicar no botão "Start / Stop"
  $scope.startStopAction = function () {
    running = !running;

    if(running){
      lastTimestamp = moment();
      intervalHandler = $interval(updateTime, 200);
      $scope.startStopBtnText = 'PAUSAR';
    }else{
      $interval.cancel(intervalHandler);
      $scope.startStopBtnText = 'INICIAR';
      updateTime();
    }
  }

  // Função chamada ao clicar no botão "Reset"
  $scope.resetAction = function () {
    $interval.cancel(intervalHandler);
    $scope.startStopBtnText = 'INICIAR';
    milliseconds = 0;
    updateDisplay();
  }

  // Atualiza valor interno do cronômetro
  function updateTime() {
    var now = moment();
    milliseconds += now.diff(lastTimestamp);
    lastTimestamp = now;

    updateDisplay();
  }

  // Atualiza display do cronômetro
  function updateDisplay() {

    var seconds = String(moment.duration(milliseconds).seconds());
    var minutes = String(moment.duration(milliseconds).minutes());

    $scope.segundos = seconds.length == 1? '0'+seconds : seconds;
    $scope.minutos  = minutes.length == 1? '0'+minutes : minutes;
  }

  ///

  $scope.pontos01 = 0;
  $scope.pontos02 = 0;

  $scope.addPontos01 = function () {
    $scope.pontos01 = $scope.pontos01 + 1;
  };

  $scope.removePontos01 = function () {
    $scope.pontos01 = $scope.pontos01 - 1;
    if( $scope.pontos01 < 0){
      $scope.pontos01 = 0;
    }
  };

  $scope.addPontos02 = function () {
    $scope.pontos02 = $scope.pontos02 + 1;
  };

  $scope.removePontos02 = function () {
    $scope.pontos02 = $scope.pontos02 - 1;
    if( $scope.pontos02 < 0){
      $scope.pontos02 = 0;
    }
  };


  $scope.salvarConfronto = function(confronto){
    $interval.cancel(intervalHandler);
    $scope.startStopBtnText = 'INICIAR';
    milliseconds = 0;
    updateDisplay();
    confronto.partida_uid = id;
    confronto.pontos01 = $scope.pontos01;
    confronto.pontos02 = $scope.pontos02;

    var ref = firebase.database().ref().child('Times');
    var query = ref.orderByChild('nome').equalTo(confronto.time01);
    var log = $firebaseArray(query);
    log.$loaded().then(function(){ 
        var obj01 = log[0];       
        var ref2 = firebase.database().ref().child('Times');
        var query2 = ref2.orderByChild('nome').equalTo(confronto.time02);
        var log2 = $firebaseArray(query2);
        log.$loaded().then(function(){ 
          var obj02 = log2[0]; 
          var count = 0;  
          angular.forEach(obj01.Jogadores, function(value1, key1) {
            angular.forEach(obj02.Jogadores, function(value2, key2) {
                if (value1.nome === value2.nome) {
                  count = count + 1;
                }
            });
          });

          if(count > 0){
            $ionicPopup.alert({
              title: 'OPS!',
              template: 'Os times possuem jogadores iguais!'
            });
          }else{
              var ref3 = firebase.database().ref().child('Confrontos');
              $firebaseArray(ref3).$add(confronto);
              $state.go('confrontosPartida', {id: confronto.partida_uid});
          }
        })  
    }) 
  }
})

app.controller('eventPartidasCtrl', function($scope,$state,$firebaseObject, $firebaseArray,$stateParams){
  var id = $stateParams.id;
  var ref = firebase.database().ref().child('Partidas');
  var query = ref.orderByChild('evento_uid').equalTo(id); 

  $scope.Partidas = $firebaseArray(query);


  $scope.addPartida = function(){
    var refEvento = firebase.database().ref().child('Groups').child(id);
    var Evento = $firebaseObject(refEvento);
    Evento.$loaded().then(function(){ 
      var Horario = Evento.Hora;
      var d = new Date();
      var mes = d.getMonth() + 1;
      var name = 'Jogos do dia '+d.getDate()+'/'+mes+' ás '+ Horario;
  
      nova = {
        evento_uid: id,
        nome: name
      }
      
      var ref = firebase.database().ref().child('Partidas');
      $firebaseArray(ref).$add(nova);
    })
  }

  $scope.acessarPartida =  function(id_partida){
    $state.go('partidaHome', {id: id_partida});
  }

  $scope.apagar = function(id){
    //1.Recuperar o objeto pelo id
    var obj = $scope.Partidas.$getRecord(id);

    //2.Exclusão pelo objeto
    $scope.Partidas.$remove(obj);
  }
  
  
})

app.controller('eventPlayersCtrl', function($scope,$state,$firebaseObject, $firebaseArray,$ionicActionSheet,$timeout,$stateParams){
  var id = $stateParams.id;
  var ref = firebase.database().ref().child('Jogadores');
  var query = ref.orderByChild('group_uid').equalTo(id); 

  $scope.Jogadores = $firebaseArray(query);
  $scope.grupo = {
    uid: id
  };

  $scope.apagar = function(id){
    //1. Recuperar o Objeto pelo Id:
    var obj = $scope.Jogadores.$getRecord(id);

    //2. Exclusão pelo Objeto
    $scope.Jogadores.$remove(obj);
  }

  $scope.editarPlayers = function(id){
    $state.go('editPlayers', {id: id});
  }

 // Triggered on a button click, or some other target
 $scope.show = function(id_jogador) {
  
     // Show the action sheet
     var hideSheet = $ionicActionSheet.show({
       buttons: [
         { text: (ionic.Platform.isAndroid()?'<i class="icon ion-edit positive"></i> ':'')+'EDITAR' },
         { text: (ionic.Platform.isAndroid()?'<i class="icon ion-cash balanced"></i> ':'')+'PAGAMENTOS' }
       ],
       //destructiveText: 'REMOVER',
       destructiveText: (ionic.Platform.isAndroid()?'<i class="icon ion-trash-a assertive"></i> ':'')+'REMOVER',
       titleText: 'Gerenciar Jogador',
       cancelText: 'Cancelar',
       cancel: function() {
            // add cancel code..
          },
       buttonClicked: function(index) {        
          if(index === 0){
            $state.go('editPlayers', {id: id_jogador});
          }else if(index === 1){
            $state.go('pagamentosPlayer', {id: id_jogador});
          }
          return true;
       },
       destructiveButtonClicked: function() {
          var obj = $scope.Jogadores.$getRecord(id_jogador);        
          //2. Exclusão pelo Objeto
          $scope.Jogadores.$remove(obj);
       }
     });
  
     // For example's sake, hide the sheet after two seconds
     $timeout(function() {
       hideSheet();
     }, 2000);
  
  };

})

app.controller('pagamentosPlayerCtrl', function($scope, $firebaseArray, $state, $firebaseAuth,$stateParams,$ionicModal) { 
  var id = $stateParams.id;
  var ref = firebase.database().ref().child('Pagamentos');
  var query = ref.orderByChild('jogador_uid').equalTo(id); 

  $scope.Pagamentos = $firebaseArray(query);

  $scope.apagar = function(id){
    //1. Recuperar o Objeto pelo Id:
    var obj = $scope.Pagamentos.$getRecord(id);

    //2. Exclusão pelo Objeto
    $scope.Pagamentos.$remove(obj);
  }

  $ionicModal.fromTemplateUrl('pagamento.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });
  $scope.openModal = function() {
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    $scope.modal.hide();
  };
  // Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });

  $scope.addPag = function(Pagamento){    
      Pagamento.jogador_uid = id;
      var mes = Pagamento.data.getMonth() + 1;
      Pagamento.Dia = Pagamento.data.getDate()+"/"+mes+"/"+Pagamento.data.getFullYear();

      //Pagamento.data = new Date(parseInt(Pagamento.data));

      var ref = firebase.database().ref().child('Pagamentos');
      $firebaseArray(ref).$add(Pagamento);
    
      $scope.modal.hide();
  }  

});

app.controller('LoginCtrl', function($scope,$state,$firebaseAuth, $ionicPopup){
  // $firebaseAuth().$onAuthStateChanged(function(firebaseUser){
  //   if(firebaseUser){
  //     $state.go('tabs.dashboard');
  //   }
  // })
  $scope.entrar = function(user){
     $firebaseAuth().$signInWithEmailAndPassword(user.email, user.password)
     .then(function(firebaseUser){
       $state.go('tabs.events');
     })
     .catch(function(error){
       alert(error.message);
     })
   }

   $scope.showReset = function() {

    $scope.data = {};

    var confirmPopup = $ionicPopup.confirm({
      template: '<input type="email" ng-model="data.email">',
      title: '<b>Esqueci minha senha</b>',
      subTitle: 'Digite seu e-mail para que possamos te enviar uma senha nova',
      scope: $scope,
      buttons: [
        { text: 'Cancelar' },
        {text: 'Confirmar',
          type: 'button-balanced',
          onTap: function(e) {
            var auth = firebase.auth();
            var email = $scope.data.email;
            
            auth.sendPasswordResetEmail(email).then(function() {
              var alertPopup = $ionicPopup.alert({
                title: 'Sucesso!',
                template: '<center>E-mail com a senha redefinida foi enviado!</center>'
              });
            }).catch(function(error){
              alert(error.message);
            })
          }
        }
      ]
    });
  };
})

app.controller('CadastroCtrl', function($scope, $firebaseArray, $state, $firebaseAuth) { 

    $scope.registrar = function(user){
      $firebaseAuth().$createUserWithEmailAndPassword(user.email, user.password)
      .then(function(firebaseUser){
        firebaseUser.sendEmailVerification();
        $state.go('login');
      })
      .catch(function(error){
        alert(error.message);
      })
    }
});

function addZero(i) {
  if (i < 10) {
      i = "0" + i;
  }
  return i;
}

app.controller('CadastroGrupoCtrl', function($scope, $firebaseArray, $firebaseAuth,$state) {
    

  
    $scope.salvarGrupo = function(grupo){
      $firebaseAuth().$onAuthStateChanged(function(firebaseUser){
        
        grupo.uid =  firebaseUser.uid;

        grupo.Hora = addZero(grupo.Time.getHours()) +":"+ addZero(grupo.Time.getMinutes());

        var ref = firebase.database().ref().child('Groups');
        $firebaseArray(ref).$add(grupo);

        var gpnome = grupo.Name;
        var d = new Date();

        alerts = {
          status: 1,
          title: gpnome,
          desc: " foi criado ás ",
          hora: addZero(d.getHours())+":"+addZero(d.getMinutes())+" em "+addZero(d.getDate())+"/"+addZero(d.getMonth()+1)+"/"+d.getFullYear(),
          uid: firebaseUser.uid
        };
        var ref = firebase.database().ref().child('Alerts');
        $firebaseArray(ref).$add(alerts);
      
        $state.go('tabs.events');
     });
    }

    $scope.meusEventos = function(){
      $state.go('tabs.events');
    }
     
});

app.controller('AlertsCtrl', function ($scope, $firebaseArray, $firebaseAuth, $state) {

  $firebaseAuth().$onAuthStateChanged(function (firebaseUser) {
    var ref = firebase.database().ref().child('Alerts');
    var query = ref.orderByChild('uid').equalTo(firebaseUser.uid);
    $scope.alerts = $firebaseArray(query);

  });

  $scope.apagar = function(id){
    //1. Recuperar o Objeto pelo Id:
    var obj = $scope.alerts.$getRecord(id);

    //2. Exclusão pelo Objeto
    $scope.alerts.$remove(obj);
  }

});
  
app.controller('EventCtrl', function($scope, $firebaseArray, $firebaseAuth,$firebaseObject, $state){
  
  $firebaseAuth().$onAuthStateChanged(function(firebaseUser){
    var ref = firebase.database().ref().child('Groups');
    var query = ref.orderByChild('uid').equalTo(firebaseUser.uid);
    $scope.Groups = $firebaseArray(query); 
  });
    
    $scope.apagar = function(id){
      //1. Recuperar o Objeto pelo Id:
      var obj = $scope.Groups.$getRecord(id);
  
      //2. Exclusão pelo Objeto
      $scope.Groups.$remove(obj);
    }

    $scope.acessar = function(id){
      $state.go('eventHome', {id: id});
    }

    $scope.verificarPags = function(id){
      var jogs = firebase.database().ref().child('Jogadores');
      var queryJogs = jogs.orderByChild('group_uid').equalTo(id);
    
      var players = $firebaseArray(queryJogs);
    
      players.$loaded().then(function(){ 
    
        angular.forEach(players, function(value1, key1) {
          var presen = firebase.database().ref().child('Presencas');
          var queryPresen = presen.orderByChild('id_jogador').equalTo(value1.$id);
          var presencas = $firebaseArray(queryPresen);
          presencas.$loaded().then(function(){
    
            var pag = firebase.database().ref().child('Pagamentos');
            var queryPag = pag.orderByChild('jogador_uid').equalTo(value1.$id);
            var pagamentos = $firebaseArray(queryPag);
    
            pagamentos.$loaded().then(function(){
    
              if(presencas.length >= 4 && pagamentos.length <= 0)
                {
                  var ref3 = firebase.database().ref().child('Groups').child(id);
                  var gp =  $firebaseObject(ref3);
    
                  gp.$loaded().then(function(){
                    var alerts = {
                      status: 2,
                      title: "O pagamento do",
                      desc: "jogador "+value1.nome+" "+value1.sobrenome,
                      hora: " está atrasado!",
                      uid: gp.uid
                    };
              
                    var ale = firebase.database().ref().child('Alerts');
                    $firebaseArray(ale).$add(alerts);
                  })
                }
            })
          })
        });
      })
    }
  });

  app.controller('EditaCtrl', function($scope, $firebaseObject, $state, $stateParams){
    var id = $stateParams.id;
    var ref = firebase.database().ref().child('Groups').child(id);
    var obj = $firebaseObject(ref);
    obj.$loaded().then(function(){
      var data = "October 13, 2014 "+obj.Hora+":00";
      obj.Time = new Date(data);
      $scope.grupo = obj;
   }) 

    //$scope.grupo = $firebaseObject(ref);

    $scope.salvarGrupo = function(grupo){
      grupo.Hora = addZero(grupo.Time.getHours()) +":"+ addZero(grupo.Time.getMinutes());
      $scope.grupo = grupo;
      $scope.grupo.$save();
      $state.go('tabs.events')
    }
  });

  app.controller('SettingCtrl', function($scope, $firebaseAuth, $firebaseArray, $state, $ionicPopup){
    $scope.showConfirm = function() {
      var confirmPopup = $ionicPopup.confirm({
        title: '<img src="img/sadball2001.png" class="sad-ball">',
        template: '<center>Você vai perder tudo!<br/> Deseja realmente deletar a conta?</center>',
        buttons: [
          {text: 'Cancelar',type:'button-assertive'},
          {text: 'Confirmar',type:'button-balanced',onTap:function(){
            var user = firebase.auth().currentUser;
            
            user.delete()
            .then(function() {
                var alertPopup = $ionicPopup.alert({
                  title: '<img src="img/sadball2002.png" class="sad-ball">',
                  template: '<center>Sério? Estou desapontado<br/>Mas sua conta foi deletada!</center>'
                });
                alertPopup.then(function(res) {
                  $state.go('inicio')
                });              
            })
            .catch(function(error) {
              var alertPopup = $ionicPopup.alert({
                title: 'Whoops!',
                template: '<center>Alguma coisa deu errado...</center>'
              });
            });
          }}
        ]
      });
    };

    $scope.showLogout= function() {
      var confirmPopup = $ionicPopup.confirm({
        title: '<img src="img/byeball.png" class="sad-ball">',
        template: '<center>Você realmente quer sair?</center>',
        buttons: [
          {text: 'Cancelar',type:'button-assertive'},
          {text: 'Confirmar',type:'button-balanced',onTap:function(){
            var ref = null;
            firebase.auth().signOut();

            $state.go('inicio')      
          }}
        ]
      });
    };

    $scope.showRePass = function(){
       var user = firebase.auth().currentUser;
        email = user.email;
        emailVerified = user.emailVerified;
        
        if(emailVerified){
          email = user.email;
          var auth = firebase.auth();
          auth.sendPasswordResetEmail(email).then(function() {
            var alertPopup = $ionicPopup.alert({
              title: 'Sucesso!',
              template: '<center>E-mail para alterar a senha foi enviado!</center>'
            });
          }).catch(function(error) {
            var alertPopup = $ionicPopup.alert({
              title: 'Whoops!',
              template: '<center>Alguma coisa deu errado...</center>'
            });
          });          
        }
        else{
          var alertPopup = $ionicPopup.alert({
            title: '<img src="img/grrball.png" class="sad-ball">',
            template: '<center>Cara, eu não posso te ajudar.<br/> Você não confirmou seu e-mail!</center>'
          });
        }
    }
  });

app.filter('hhmmss', function () {
  return function (time) {
    var sec_num = parseInt(time, 10); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    var time    = hours+':'+minutes+':'+seconds;
    return time;
  }
});